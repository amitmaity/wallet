<?php
namespace Drupal\wallet\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Wallet controller.
 */
class Wallet extends ControllerBase
{

  /**
   * {@inheritdoc}
   */
  public function content()
  {
    $urls = array();
    $urls['currency'] = Url::fromRoute('wallet.wallet_currency_settings', array());
    $urls['category'] = Url::fromRoute('wallet.wallet_category_settings', array());
    $urls['transaction'] = Url::fromRoute('wallet.wallet_transaction_settings', array());
    $urls['currency_list'] = Url::fromRoute('entity.wallet_currency.collection', array());
    $urls['category_list'] = Url::fromRoute('entity.wallet_category.collection', array());
    $urls['transaction_list'] = Url::fromRoute('entity.wallet_transaction.collection', array());
    foreach ($urls as $key => $value) {

      if (strpos($key, '_') !== false) {
        $description = 'List of content of  ' . ucwords(str_replace('_', ' ', $key)) . ' Entity';
        $title = ucwords(str_replace('_', ' ', $key));
      } else {

        $description = 'Provide entity settings of ' . ucwords(str_replace('_', ' ', $key));
        $title = ucwords(str_replace('_', ' ', $key)) . ' Settings';
      }
      $block['title'] = '';
      $block['description'] = '';


      $block['content'] = array('#theme' => 'admin_block_content', '#content' => array($key => array('title' => $title, 'description' => $description, 'url' => $value)),);
      if (!empty($block['content']['#content'])) {
        $blocks[$key] = $block;
      }

    }


    if ($blocks) {
      ksort($blocks);
      $build = ['#theme' => 'admin_page', '#blocks' => $blocks,];
      return $build;
    }
  }

}


