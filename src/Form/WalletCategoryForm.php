<?php

namespace Drupal\wallet\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;

/**
 * Form controller for the wallet_category entity edit forms.
 *
 * @ingroup wallet_category
 */
class WalletCategoryForm extends ContentEntityForm
{

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /* @var $entity \Drupal\wallet_category\Entity\WalletCategory */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $form['langcode'] = array('#title' => $this->t('Language'), '#type' => 'language_select', '#default_value' => $entity->getUntranslated()->language()->getId(), '#languages' => Language::STATE_ALL,);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state)
  {
    $form_state->setRedirect('view.list_category.page_1');
    $entity = $this->getEntity();
    $entity->save();
  }
}
