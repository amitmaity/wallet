<?php

namespace Drupal\wallet\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class WalletCurrencySettingsForm.
 *
 * @package Drupal\wallet_currency\Form
 * @ingroup wallet_currency
 */
class WalletCurrencySettingsForm extends FormBase
{
  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'wallet_currency_entity_settings';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    // Empty implementation of the abstract submit class.
  }


  /**
   * Define the form used for WalletCurrency settings.
   *
   * @return array
   *   Form definition array.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['wallet_currency_settings']['#markup'] = 'Settings form for Wallet Currency. Manage field settings here.';
    return $form;
  }
}
